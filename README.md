# READ ME #

## extends_NCM modules ##

the extends_NCM modules are extentions to some of the native Xojo classes.  these were written over the past decade of using Xojo/REALBasic and are just collected in one place for ease of use.

* `string` (old framework)
* `text` (new framework)
* `date` (old framework)
* `integer` (old framework)
* others....

### testing ###
currently there is testing on some of the extentions.  more will come soon(tm).

### optimization ###
currently the methods are not optimized.  All methods are using the first try to complete the method over using the best method.  Optimizations will come in the future.

### sample code ###
there is no sample code at this time.  the closest to sample code is the Xojo Unit Tests.

### version history ###
* 2018-02-22 - initial commit (for public sharing).
