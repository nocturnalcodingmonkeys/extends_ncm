# string_NCM

## methods

* ChompNCM
* ChompSpecialCharacterNCM( string )
* ContainsStringNCM (string )
* CountCharactersNCM
* HasCharacterNCM
* HasLowerNCM
* HasNumberNCM
* HasPunctuationNCM
* HasUpperNCM
* HasWhiteSpaceNCM
* IsCharacterNCM
* IsEmailAddressNCM
* IsIpAddressV4NCM
* IsLowerNCM
* IsPunctuationNCM
* IsUpperNCM
* IsWhitespaceNCM
* LongestWordInStringNCM
* paddedWithSpacesNCM( integer )
* prePaddeWithSpacesNCM( integer )
* ShortestWordInStringNCM
