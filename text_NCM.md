# text_NCM

## methods

* ChompNCM
* ChompSpecialCharacterNCM( text )
* CountCharactersNCM
* HasCharacterNCM
* HasLowerNCM
* HasNumberNCM
* HasPunctuationNCM
* HasUpperNCM
* HasWhiteSpaceNCM
* IsCharacterNCM
* IsLowerNCM
* IsPunctuationNCM
* IsUpperNCM
* IsWhitespaceNCM
* LongestWordInStringNCM
* paddedWithSpacesNCM( integer )
* prePaddeWithSpacesNCM( integer )
* ShortestWordInStringNCM
